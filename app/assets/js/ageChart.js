import Chart from 'chart.js/auto';
import $ from "jquery";


const DATA_COUNT = 5;
const NUMBER_CFG = {count: DATA_COUNT, min: 0, max: 100};

const data = {
    labels: ['0 - 15r', '16 - 25r', '26-36', '36-50r', '50+ r'],
    datasets: [
        {
            label: 'Dataset 1',
            data: [20,20,20,20,20],
            backgroundColor: [
                'red',
                'green',
                'blue',
                'orange',
                'purple'
            ]
        }
    ]
};

const config = {
    type: 'pie',
    data: data,
    options: {
        responsive: true,
        plugins: {
            legend: {
                position: 'top',
            },
            title: {
                display: true,
                text: 'Percentage employee age'
            }
        }
    },
};

let myChartBasic;

if ($("#chartAgeBasic").length > 0) {
    let ctx = document.getElementById('chartAgeBasic').getContext('2d');
    myChartBasic = new Chart(ctx, config);
    realoadGraph();
}


function realoadGraph() {
    let request = $.ajax({
        url: "/api/employee/average-age",
        method: "POST",
        data: {

        },
        dataType: "json"
    });

    request.done(function( response ) {
        if(response.status === 'ok') {

            let chartInfo = $("#chartAddInfo");

            myChartBasic.data.datasets[0].data = [];
            myChartBasic.data.labels = [];
            for (let key in response.percentageAgeData) {
                myChartBasic.data.datasets[0].data.push(response.percentageAgeData[key]);
                myChartBasic.data.labels.push(key);
                chartInfo.append('<tr><td>'+key+'</td><td>'+response.percentageAgeData[key]+' %</td></tr>');
            }
            myChartBasic.update();
        }
    });
}

export { realoadGraph };