import $ from 'jquery';
import { realoadGraph } from './ageChart';

$( document ).ready(function() {

    $("body").on('click', '.js_DeleteEmployee', function (event) {
        const line = $(this).closest('tr');
        const id = $(this).attr('data-id');

        if (window.confirm($(this).attr('data-alert-message')) === false) {
            return;
        }

        $.ajax({
            url: '/api/employee/delete/' + id,
            type: 'DELETE',
            success: function(result) {
                line.fadeOut('slow', function() {
                    $(this).remove();
                    realoadGraph();
                });
            },
            fail: function(result) {
                alert('Request failed: ' + result.message);
            }
        });
    });

});
