<?php

namespace App\Controller\Api;

use App\Service\EmployeeService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api/employee', name: 'app_api_employee')]
class EmployeeApiController extends AbstractController
{
    private EmployeeService $employeeService;

    public function __construct(EmployeeService $employeeService)
    {
        $this->employeeService = $employeeService;
    }

    #[Route('/delete/{id}', name: 'app_employee_delete', methods: ['DELETE'])]
    public function delete(string $id = ''): JsonResponse
    {
        if ('' === $id) {
            return new JsonResponse(['status' => 'error', 'message' => 'Id is empty'], 400);
        }

        try {
            $this->employeeService->deleteById($id);
        } catch (\Exception $e) {
            return new JsonResponse(['status' => 'error', 'message' => $e->getMessage()], 500);
        }

        return new JsonResponse(['status' => 'ok']);
    }

    #[Route('/average-age', name: 'app_employee_average_age', methods: ['POST'])]
    public function averageAge(): JsonResponse
    {
        try {
            $percentageAgeData = $this->employeeService->percentageGroupData();
        } catch (\Exception $e) {
            return new JsonResponse(['status' => 'error', 'message' => $e->getMessage()], 500);
        }

        return new JsonResponse(
            [
                'status' => 'ok',
                'percentageAgeData' => $percentageAgeData,
            ]
        );
    }
}
