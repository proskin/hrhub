<?php

namespace App\Controller;

use App\Dto\EmployeeFormDto;
use App\Form\EmployeeFormType;
use App\Service\EmployeeService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/employee')]
class EmployeeController extends AbstractController
{
    private EmployeeService $employeeService;

    public function __construct(EmployeeService $employeeService)
    {
        $this->employeeService = $employeeService;
    }

    #[Route('', name: 'app_employee')]
    public function index(): Response
    {
        return $this->render('employee/index.html.twig', [
            'employeeList' => $this->employeeService->fetchAllAsDtoShow(),
        ]);
    }

    #[Route(['/edit/{id}'], name: 'app_employee_save')]
    public function editOrAdd(Request $request, string $id = ''): Response
    {
        $employee = new EmployeeFormDto();
        if ('' !== $id) {
            $employee = $this->employeeService->getById($id);
        }

        $form = $this->createForm(EmployeeFormType::class, $employee);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->employeeService->saveFromDto($form->getData());

            $this->addFlash('success', 'Save successfully!');

            return $this->redirectToRoute('app_employee');
        }

        return $this->render('employee/save.html.twig', [
            'form' => $form->createView(),
            'employee' => $employee,
        ]);
    }
}
