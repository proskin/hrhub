<?php

namespace App\Dto;

class EmployeeFormDto
{
    private ?string $id = '';

    private ?string $name = null;

    private ?int $gender = null;

    private ?string $dateOfBirth = null;

    /**
     * @throws \Exception
     */
    public static function fromEmployeeShowDto(EmployeeShowDto $dto): self
    {
        $employee = new self();

        $employee->setId($dto->getId());
        $employee->setGender($dto->getGender());
        $employee->setName($dto->getName());
        $employee->setGender($dto->getGender());
        $employee->setDateOfBirth($dto->getDateOfBirth());

        return $employee;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function setId(?string $id): void
    {
        $this->id = $id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    public function getGender(): ?int
    {
        return $this->gender;
    }

    public function setGender(?int $gender): void
    {
        $this->gender = $gender;
    }

    public function getDateOfBirth(): ?string
    {
        return $this->dateOfBirth;
    }

    public function setDateOfBirth(?string $dateOfBirth): void
    {
        $this->dateOfBirth = $dateOfBirth;
    }
}
