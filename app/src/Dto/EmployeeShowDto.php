<?php

namespace App\Dto;

class EmployeeShowDto
{
    public const GENDER_WOMAN = 1;
    public const GENDER_MAN = 2;
    public const GENDER_OTHER = 3;

    private string $id;

    private string $name;

    private int $gender;

    private string $dateOfBirth;

    private int $age;

    /**
     * @throws \Exception
     */
    public static function fromEmployeeFormDto(EmployeeFormDto $employee): self
    {
        $employeeShowDto = new self();
        $employeeShowDto->id = $employee->getId();
        $employeeShowDto->gender = $employee->getGender();
        $employeeShowDto->name = $employee->getName();
        $employeeShowDto->dateOfBirth = $employee->getDateOfBirth();

        $birthDate = new \DateTimeImmutable($employee->getDateOfBirth());
        $currentDate = new \DateTimeImmutable();
        $employeeShowDto->age = $currentDate->diff($birthDate)->y;

        return $employeeShowDto;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getGender(): int
    {
        return $this->gender;
    }

    public function getAge(): int
    {
        return $this->age;
    }

    public function getDateOfBirth(): string
    {
        return $this->dateOfBirth;
    }

    public function setDateOfBirth(string $dateOfBirth): void
    {
        $this->dateOfBirth = $dateOfBirth;
    }
}
