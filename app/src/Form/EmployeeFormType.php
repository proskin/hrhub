<?php

namespace App\Form;

use App\Dto\EmployeeFormDto;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class EmployeeFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('id', HiddenType::class)
            ->add('name', TextType::class, [
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please enter a title',
                    ]),
                    new Length([
                        'min' => 2,
                        'maxMessage' => 'Your title should be at max {{ limit }} characters',
                        'minMessage' => 'Your title should be at min {{ limit }} characters',
                        'max' => 255,
                    ]),
                ],
            ])->add('gender', ChoiceType::class, [
                // boolean is also possible, but not at this epoch :D
                'required' => true,
                'label' => 'Gender',
                'choices' => [
                   'Woman' => 1,
                   'Man' => 2,
                   'Other' => 3,
                   'Hybrid' => 4,
                ],
            ])->add('dateOfBirth', DateType::class, [
                'required' => true,
                'label' => 'Birthdate',
                'input' => 'string',
                'years' => range(date('Y') - 66, date('Y') - 1),
                'placeholder' => [
                    'year' => 'Year', 'month' => 'Month', 'day' => 'Day',
                ],
            ])->add('save', SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => EmployeeFormDto::class,
        ]);
    }
}
