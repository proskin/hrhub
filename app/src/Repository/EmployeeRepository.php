<?php

namespace App\Repository;

use App\Dto\EmployeeFormDto;
use App\Service\DtoEmployeeSerializer;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;

class EmployeeRepository
{
    private string $saveXmlPath;
    private DtoEmployeeSerializer $serializer;

    public function __construct(string $saveXmlPath, DtoEmployeeSerializer $serializer)
    {
        $this->saveXmlPath = $saveXmlPath;
        $this->serializer = $serializer;
    }

    /**
     * @return array<EmployeeFormDto>
     */
    public function fetchAll(): array
    {
        try {
            $xmlAsArray = $this->serializer->decode(file_get_contents($this->saveXmlPath), 'xml');

            if (false === is_iterable($xmlAsArray)) {
                return [];
            }

            return array_map(function ($item) {
                return $this->serializer->denormalize($item, EmployeeFormDto::class, 'xml');
            }, $xmlAsArray);
        } catch (NotEncodableValueException $e) {
            return [];
        }
    }

    public function addFromForm(EmployeeFormDto $employeeFormDto): void
    {
        $employeeList = $this->fetchAll();
        $employeeList[] = $employeeFormDto;

        $this->saveXml($employeeList);
    }

    public function updateFromForm(EmployeeFormDto $dto): void
    {
        $employeeList = $this->fetchAll();
        foreach ($employeeList as $key => $employee) {
            if ($employee->getId() === $dto->getId()) {
                $employeeList[$key] = $dto;
            }
        }

        $this->saveXml($employeeList);
    }

    /**
     * @param array<EmployeeFormDto> $employeeList
     */
    private function saveXml(array $employeeList): void
    {
        $data = $this->serializer->serialize($employeeList, 'xml');

        file_put_contents($this->saveXmlPath, $data, LOCK_EX);
    }

    /**
     * @throws \Exception
     */
    public function findById(string $id): ?EmployeeFormDto
    {
        $employeeList = $this->fetchAll();

        foreach ($employeeList as $employee) {
            if ($employee->getId() === $id) {
                return $employee;
            }
        }

        return null;
    }

    public function deleteById(string $id): void
    {
        $employeeList = $this->fetchAll();

        foreach ($employeeList as $key => $employee) {
            if ($employee->getId() === $id) {
                unset($employeeList[$key]);
            }
        }

        $this->saveXml($employeeList);
    }

    public function truncate(): void
    {
        // $this->saveXml([]);
        file_put_contents($this->saveXmlPath, '', LOCK_EX);
    }
}
