<?php

namespace App\Service;

use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Normalizer\PropertyNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;

class DtoEmployeeSerializer implements SerializerInterface
{
    private Serializer $serializer;

    public function __construct()
    {
        $normalizers = [
            new DateTimeNormalizer(),
            new ArrayDenormalizer(),
            new PropertyNormalizer(),
            new ObjectNormalizer(propertyTypeExtractor: new ReflectionExtractor()),
        ];

        $this->serializer = new Serializer(
            $normalizers, [new XmlEncoder()]
        );
    }

    public function serialize(mixed $data, string $format, array $context = []): string
    {
        return $this->serializer->serialize($data, $format, $context);
    }

    public function deserialize(mixed $data, string $type, string $format, array $context = []): mixed
    {
        return $this->serializer->deserialize($data, $type, $format, $context);
    }

    /**
     * @param array<mixed> $context
     *
     * @return array<mixed>
     */
    public function decode(string $data, string $format, array $context = []): array
    {
        return $this->serializer->decode($data, $format, $context);
    }

    public function denormalize(mixed $employee, string $class, string $string): mixed
    {
        return $this->serializer->denormalize($employee, $class, $string);
    }
}
