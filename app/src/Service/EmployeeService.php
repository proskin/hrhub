<?php

namespace App\Service;

use App\Dto\EmployeeFormDto;
use App\Dto\EmployeeShowDto;
use App\Repository\EmployeeRepository;
use Symfony\Component\Uid\Uuid;

class EmployeeService
{
    private EmployeeRepository $employeeRepository;

    public function __construct(EmployeeRepository $employeeRepository)
    {
        $this->employeeRepository = $employeeRepository;
    }

    public function saveFromDto(EmployeeFormDto $dto): void
    {
        if (null === $dto->getId()) {
            $dto->setId(Uuid::v7()->toRfc4122());
            $this->employeeRepository->addFromForm($dto);

            return;
        }

        $this->employeeRepository->updateFromForm($dto);
    }

    /**
     * @return array<EmployeeFormDto>
     *
     * @throws \Exception
     */
    public function fetchAll(): array
    {
        return $this->employeeRepository->fetchAll();
    }

    /**
     * @throws \Exception
     */
    public function getById(string $id): ?EmployeeFormDto
    {
        return $this->employeeRepository->findById($id);
    }

    /**
     * @return array<EmployeeShowDto>
     *
     * @throws \Exception
     */
    public function fetchAllAsDtoShow(): array
    {
        $employeeList = $this->employeeRepository->fetchAll();

        return array_map(function ($employee) {
            return EmployeeShowDto::fromEmployeeFormDto($employee);
        }, $employeeList);
    }

    public function deleteById(string $id): void
    {
        $this->employeeRepository->deleteById($id);
    }

    /**
     * @return array<string,int>
     *
     * @throws \Exception
     */
    public function percentageGroupData(): array
    {
        $employeeList = $this->fetchAllAsDtoShow();
        $totalEmployees = count($employeeList);

        $averageData = [
           '0 - 25 r' => 0,
           '26 - 35 r' => 0,
           '36 - 45 r' => 0,
           '46 - 55 r' => 0,
           '56+' => 0,
        ];

        if (0 === $totalEmployees) {
            return $averageData;
        }

        foreach ($employeeList as $employeeFormDto) {
            $age = $employeeFormDto->getAge();

            if ($age >= 0 && $age <= 25) {
                ++$averageData['0 - 25 r'];
            } elseif ($age >= 26 && $age <= 35) {
                ++$averageData['26 - 35 r'];
            } elseif ($age >= 36 && $age <= 45) {
                ++$averageData['36 - 45 r'];
            } elseif ($age >= 46 && $age <= 55) {
                ++$averageData['46 - 55 r'];
            } else {
                ++$averageData['56+'];
            }
        }

        foreach ($averageData as $ageGroup => $count) {
            if (0 === $count) {
                continue;
            }

            $percentage = ($count / $totalEmployees) * 100;
            $averageData[$ageGroup] = round($percentage);
        }

        return $averageData;
    }
}
