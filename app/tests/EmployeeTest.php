<?php

namespace App\Tests;

use App\Dto\EmployeeFormDto;
use App\Dto\EmployeeShowDto;
use DateTime;
use PHPUnit\Framework\TestCase;

class EmployeeTest extends TestCase
{
    public function testSomething(): void
    {
        $this->assertTrue(true);
    }

    public function test_transfer_EmployeeFormDto_to_EmployeeShowDto(): void
    {
        $formDto = new EmployeeFormDto();
        $formDto->setId('123');
        $formDto->setName('John');
        $formDto->setGender(1);

        $date = new DateTime('now');
        $date->modify('-3 years');

        $formDto->setDateOfBirth(date($date->format('Y-m-d')));

        $showDto = EmployeeShowDto::fromEmployeeFormDto($formDto);

        $this->assertEquals($formDto->getId(), $showDto->getId());
        $this->assertEquals($formDto->getName(), $showDto->getName());
        $this->assertEquals($formDto->getGender(), $showDto->getGender());
        $this->assertEquals($formDto->getDateOfBirth(), $showDto->getDateOfBirth());
        $this->assertEquals(3, $showDto->getAge());
    }
}
