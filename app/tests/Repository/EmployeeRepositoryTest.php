<?php

namespace App\Tests\Repository;

use App\Dto\EmployeeFormDto;
use App\Dto\EmployeeShowDto;
use App\Repository\EmployeeRepository;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class EmployeeRepositoryTest extends KernelTestCase
{
    private EmployeeRepository $employeeRepository;

    protected function setUp(): void
    {
       $kernel = self::bootKernel();
       $this->employeeRepository = $kernel->getContainer()->get(EmployeeRepository::class);
    }

    public function testAddFromForm(): void
    {
        $this->employeeRepository->truncate();

        $employeeFormDto = new EmployeeFormDto();
        $employeeFormDto->setId('123');
        $employeeFormDto->setName('John');
        $employeeFormDto->setGender(EmployeeShowDto::GENDER_MAN);
        $employeeFormDto->setDateOfBirth('2021-01-01');
        $this->employeeRepository->addFromForm($employeeFormDto);

        $employeeFormDto = new EmployeeFormDto();
        $employeeFormDto->setId('124');
        $employeeFormDto->setName('Jana');
        $employeeFormDto->setGender(EmployeeShowDto::GENDER_WOMAN);
        $employeeFormDto->setDateOfBirth('2015-01-01');
        $this->employeeRepository->addFromForm($employeeFormDto);

        $employeeFormDto = new EmployeeFormDto();
        $employeeFormDto->setId('125');
        $employeeFormDto->setName('Erika');
        $employeeFormDto->setGender(EmployeeShowDto::GENDER_WOMAN);
        $employeeFormDto->setDateOfBirth('1984-01-01');
        $this->employeeRepository->addFromForm($employeeFormDto);

        $employeeList = $this->employeeRepository->fetchAll();

        $this->assertCount(3, $employeeList);

        $this->assertEquals('123', $employeeList[0]->getId());
        $this->assertEquals('John', $employeeList[0]->getName());
        $this->assertEquals(EmployeeShowDto::GENDER_MAN, $employeeList[0]->getGender());
        $this->assertEquals('2021-01-01', $employeeList[0]->getDateOfBirth());
    }

    public function testFindById(): void
    {
        $employeeList = $this->employeeRepository->fetchAll();
        $employee = $this->employeeRepository->findById($employeeList[0]->getId());

        $this->assertEquals($employeeList[0]->getId(), $employee->getId());
    }

    public function testDeleteById(): void {
        $employeeList = $this->employeeRepository->fetchAll();
        $this->employeeRepository->deleteById($employeeList[0]->getId());

        $employee = $this->employeeRepository->findById($employeeList[0]->getId());

        $this->assertEquals(null, $employee);
    }

    public function testTruncate(): void{
        $this->employeeRepository->truncate();
        $employeeList = $this->employeeRepository->fetchAll();

        $this->assertCount(0, $employeeList);
    }
}
